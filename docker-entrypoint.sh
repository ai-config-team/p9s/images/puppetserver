#!/bin/sh
set -e

update-alternatives --set java java-1.8.0-openjdk.x86_64

echo "188.185.66.144 constable constable.cern.ch" >> /etc/hosts
echo "188.184.81.105 judy judy.cern.ch" >> /etc/hosts

exec /opt/puppetlabs/bin/puppetserver "$@"
